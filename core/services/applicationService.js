const APPLICATION_MODEL = require('../models/application');
const axios = require("axios").default;
const FormData = require('form-data');
const fs = require('fs/promises');

/**
 * This function will return every application in our database
 * 
 * @returns Application[] | Boolean
 */
 exports.applicationList = async () => {
    try {
        let data = await APPLICATION_MODEL.Application.findAll();

        return data;
    } catch(e) {
        return false;
    }
}

/**
 * This function will return a single application
 * 
 * @param {*} primaryKey 
 * @returns Application | Boolean
 */
 exports.applicationSpecific = async (primaryKey) => {
    try {
        let data = await APPLICATION_MODEL.Application.findByPk(primaryKey);

        return data;
    } catch(e) {
        return false;
    }
}

/**
 * This function will persist the file into our serve and insert into the database the data.
 * 
 * @param {*} file 
 * @param {*} fileInfos 
 * @returns Application | Boolean
 */
exports.applicationCreate = async (file, fileInfos) => {
    try {
        let hashedName = `${file.md5}.${file.name.split(".").pop()}`;
        await file.mv(`./storage/apps/${hashedName}`);

        let application = await APPLICATION_MODEL.Application.create({ 
            reference: file.md5,
            name: fileInfos.name, 
            description: fileInfos.description 
        });

        // let virusResponse = await applicationVirusVerification(`${hashedName}.${file.name.split(".").slice(-1)}`);

        return application;
    } catch(e) {
        return false;
    }
}

/**
 * This function will update the Name and Description of an application
 * 
 * @param {*} payload 
 * @returns 
 */
exports.applicationUpdate = async (payload) => {
    try {
        let application = await APPLICATION_MODEL.Application.update({ 
            name: payload.form.name,
            description: payload.form.description
        }, {
            where: {
              id: payload.application.id
            }
        });

        return application;
    } catch(e) {
        return false;
    }
}

/* -------------------------------------------------------------------------- */
/*                              UNDER DEVELOPMENT                             */
/* -------------------------------------------------------------------------- */

/**
 * This function will send an http request to the VirusTotal API and verify the integrity of the file
 * 
 * @param string fileName 
 */
exports.applicationVirusVerification = async (fileName) => {
    const FILE = await fs.readFile(`./storage/apps/${fileName}`);

    // const form = new FormData();
    // form.append('file', file, fileName);

    // const response = await axios.post('https://www.virustotal.com/api/v3/files/upload_url', form, {
    //     headers: {
    //         ...form.getHeaders(),
    //         'x-apikey': 'c81e9bbc209fd4002bc197ca2fa3fe799d4885f74bf88ed5169060165e4683de'
    //     },
    // });

    // console.log(response);

    const options = {
        method: 'POST',
        url: 'https://www.virustotal.com/api/v3/files',
        headers: {
          'x-apikey': 'c81e9bbc209fd4002bc197ca2fa3fe799d4885f74bf88ed5169060165e4683de',
          'Content-Type': 'multipart/form-data; boundary=---011000010111000001101001'
        },
        data: FILE
      };
      
      axios.request(options)
        .then(function (response) {
            console.log(response.data);
        }).catch(function (error) {
            console.error(error);
        });
}