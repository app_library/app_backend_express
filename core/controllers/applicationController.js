const APPLICATION_SERVICE = require('../services/applicationService');

// Returns all the applications in our backend
exports.applicationList = async (req, res) => {
    try {
        let applicationList = await APPLICATION_SERVICE.applicationList();
        res.send(applicationList).status(200);
    } catch (e) {
        res.send(e).status(400);
    }
}

// Returns a specific application that we choose
exports.applicationSpecific = async (req, res) => {
    try {
        let application = await APPLICATION_SERVICE.applicationSpecific(req.params.id);
        res.send(application).status(200);
    } catch (e) {
        res.send(e).status(400);
    }
}


// It will handle a creation of an application
exports.applicationCreate = async (req, res) => {
    try {
        let returnedData = await APPLICATION_SERVICE.applicationCreate(req.files.file, req.body);
        res.send(returnedData).status(200);
    } catch (e) {
        res.send(e).status(400);
    }
}

// It handles the change of the name and description of our application
exports.applicationUpdate = async (req, res) => {
    try {
        let returnedData = await APPLICATION_SERVICE.applicationUpdate(req.body);
        res.send(returnedData).status(200);
    } catch (e) {
        res.send(e).status(400);
    }
}

