const { Sequelize, Model, DataTypes } = require("sequelize");

/* -------------------------------------------------------------------------- */
/*                                INIT OPTIONS                                */
/* -------------------------------------------------------------------------- */

const sequelize = new Sequelize('postgres://postgres:root@172.17.0.1:5432/APP_LIBRARY');


async function databaseInit() {
    try {
        await sequelize.authenticate();
        await sequelize.sync({ alter: true });
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

databaseInit();

/* -------------------------------------------------------------------------- */
/*                              APPLICATION MODEL                             */
/* -------------------------------------------------------------------------- */

/**
 * Definition of our Entity Application
 */
const Application = sequelize.define('application', {
    // Model attributes are defined here
    reference: {
        type: DataTypes.STRING
    },
    name: {
        type: DataTypes.STRING
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Application.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    reference: {
        type: DataTypes.STRING
    },
    name: {
        type: DataTypes.STRING(50)
    },
    description: {
        type: DataTypes.STRING(65535),
        allowNull: false
    }
}, { sequelize });

module.exports = { Application };