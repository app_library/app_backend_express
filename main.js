// Our Express App
const express = require('express');
const app = express();
const port = 8080;

// Packages
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');

// Application Routers
const indexRouter = require('./routes/index');
const applicationRouter = require('./routes/application');


/* -------------------------------------------------------------------------- */
/*                             GENERIC MIDDLEWARES                            */
/* -------------------------------------------------------------------------- */

// Packages Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(cors());
app.use(fileUpload({ preserveExtension: true }));

// Routers Middlewares
app.use('/', indexRouter);
app.use('/application', applicationRouter);

/* -------------------------------------------------------------------------- */
/*                                    OTHER                                   */
/* -------------------------------------------------------------------------- */

// Starting Messagen to notify the listening port
const server = app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
