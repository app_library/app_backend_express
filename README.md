# Application Library (Server-Side)

## Description

The Serverside of the application was developed on a node server using the expressjs framework to help and facilitate the development itself, the architecture was developed by myself trying to anticipate as much as possible the evolution of a serverside application. I used docker to create a multi-container application to simulate distant servers, so i will explain how to set it up later on.

## Main Packages

-   **express:** The framework that we will be using during the development
-   **fileUpload:** To receive the uploaded file from the clientside
-   **nodemon:** To restart the node server each time we'll save the project
-   **sequelize:** It will help us to manipulate and persist data into our database (ORM)


## Folder Structure

```
-   core  (Where we save our main overall structural code)
    -   controllers  (It will manage our request and send an appropriate response to the clientside)
    -   models  (Our entities // ORM management)
    -   services  (Logical code)
-   routes  (Will redirect our routes to the right controller)
-   storage  (Stock everything that we want in out server)
    -   apps  (All added apps will be saved right here)
```

## Database

For testing purpose i only created one single entity (Application), so i didn't create/generate any type of database modelisation (CDM, LDM, PDM). In a real world utilisation, we should always ensure that our database are accurately represented, omission of data will lead to creation of faulty reports and produce incorrect results.

---

## Installation

Docker is an open source containerization platform that will enable us to pack our application into containers (Literally independant OS).

Docker installation Guides (If you already have docker installed, you can skip this step):
- [Docker Desktop Download](https://www.docker.com/products/docker-desktop/)
- [Install WSL 2](https://docs.microsoft.com/fr-fr/windows/wsl/install)
- [Get started with Docker](https://docs.microsoft.com/en-us/windows/wsl/tutorials/wsl-containers)
- [Container with VSCODE](https://code.visualstudio.com/docs/remote/containers-tutorial)

Installing our container as node environment
```docker
# Command to paste into your CLI after you installed the docker environment
docker run -dti --name APP_LIBRARY_SS -p 8080:8080 node
```

When you are connected to your container with vscode, you will need to pull the git repository.

```sh
# Preparation of our folders & git install & install nodemon in global
mkdir //home/my-app && cd //home/my-app
apt update && apt install git -y
npm install nodemon -g
```

```sh
# Pull our project and save the remote as origin
git init
git remote add origin https://gitlab.com/app_library/app_backend_express.git
git pull origin master
npm install
```

Even if we have an ORM that will manage our database, we still need a database container aswell

```docker
# Command to paste into your CLI 
docker run -d --name APP_LIBRARY_DB -e POSTGRES_PASSWORD=root -p 5432:5432 postgres:14.2-alpine
```

> Note: If you change any port that i used in the docker runs, dont forget to change it in code aswell or the application will not work.
---

## Running the Application


```sh
# Make sure you are on the right directory ( cd //home/my-app )
nodemon main.js
```

---

## Disclaimer

> Every time that i mention "application" i'm not referring to our "serverside application", i'm referring to the entity Application.

---

Enjoy, thanks ;)