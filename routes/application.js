const express = require('express');
const router = express.Router();

// Required controllers related to the applications
const APPLICATION_CONTROLLER = require('../core/controllers/applicationController');

/* -------------------------------------------------------------------------- */
/*                                   ROUTES                                   */
/* -------------------------------------------------------------------------- */

// Get all the applications
router.get('/', APPLICATION_CONTROLLER.applicationList);

// Get a speficif application
router.get('/:id', APPLICATION_CONTROLLER.applicationSpecific);

// Create a new application
router.post('/', APPLICATION_CONTROLLER.applicationCreate)

// Update the an existant application
router.patch('/', APPLICATION_CONTROLLER.applicationUpdate)


module.exports = router;