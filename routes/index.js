const express = require('express');
const router = express.Router();

/* -------------------------------------------------------------------------- */
/*                                   ROUTES                                   */
/* -------------------------------------------------------------------------- */

router.get('/', function(req, res) {
    res.send("You didn't specify any route").status(200);
});

module.exports = router;